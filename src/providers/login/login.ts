import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


@Injectable()
export class LoginProvider {
    rut : null;
    role : null;
    email : null; 
    apikey : null;
    birhdate : null;
    firstName: null;
    gender: null;
    lastName: null;

    apiurl = 'https://api.sebastian.cl/academia/api/v1/'


  constructor(public httpclient: HttpClient) {
    console.log('Hello LoginProvider Provider');
  }

  loggin(rut: string, password: string){
    let url = this.apiurl + 'authentication/authenticate';
    let info = {rut: rut, password: password};
    return this.httpclient.post(url,info)
  }

  forgot(rut: string){
    let url = this.apiurl + 'authentication/forgot/' + this.rut;
    return this.httpclient.post(url,{});
  }

  student(){
    let url = this.apiurl + `students/${this.rut}`;
    let httpOptions = {
      headers: new HttpHeaders({ 'X-API-KEY': localStorage.getItem("apiKey")})
    };
    return this.httpclient.get(url,httpOptions)
  }

}
