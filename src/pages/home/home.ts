import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from 'ionic-angular'

import { ListPage } from '../list/list';
import { ForgotPage } from '../forgot/forgot';
import { LoginProvider } from '../../providers/login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  myForm: FormGroup;

  constructor(public navCtrl: NavController,
              public loginprovider: LoginProvider,
              public formBuilder: FormBuilder,
              public alertcontrol: AlertController) {

                this.myForm = this.formBuilder.group({
                  rut: ['',Validators.required],
                  password: ['',Validators.required]
                });
  }

  login() {
    this.loginprovider
        .loggin(this.myForm.value.rut,this.myForm.value.password).subscribe((data:any)=>{
        console.log(data);
        this.loginprovider.rut = data.rut;
        localStorage.setItem("apiKey",data.apikey);
        this.loginprovider.apikey = data.apikey;
        this.loginprovider.role = data.role;
        this.loginprovider.email = data.email;
        this.navCtrl.setRoot(ListPage);
        }, this.failogin)
  }

  failogin(error) {
    console.log(error);
  }

  forgotpass() {
    console.log("Restablecer contraseña");
    this.navCtrl.push(ForgotPage);
}

}
