import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { LoginProvider } from '../../providers/login/login'

/*
 * Generated class for the AlumnoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-alumno',
  templateUrl: 'alumno.html',
})
export class AlumnoPage {
  nombreAlumno: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loginprovider: LoginProvider,
              public httpclient: HttpClient) {

                console.log(loginprovider)
  }

  // pie

  num1 = 0.6;
  public pieChartLabels:string[] = ['Aprobados', 'Reprobados'];
  public pieChartData:Number[] = [60,40];
  public pieChartType:string = 'pie';

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }

  ngOnInit(){;
    this.loginprovider
        .student().subscribe((data:any)=>{
        console.log(data);
      this.loginprovider.firstName = data.firstName;
      this.loginprovider.birhdate = data.birhdate;
      this.loginprovider.lastName = data.lastName;
      this.loginprovider.gender = data.gender;
      this.nombreAlumno = localStorage.getItem("apiKey");
      },
    );
  }




  ionViewDidLoad() {
    console.log('ionViewDidLoad AlumnoPage');
  }

}
