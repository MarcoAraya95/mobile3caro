import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Nav } from 'ionic-angular';

import { HomePage } from '../home/home';
import { AlumnoPage } from '../alumno/alumno';
import { ProfesorPage } from '../profesor/profesor';
import { AvisoprofePage } from '../avisoprofe/avisoprofe';
import { GrafalumnoPage } from '../grafalumno/grafalumno';
import { ListalumnoPage } from '../listalumno/listalumno';
import { ListprofesorPage } from '../listprofesor/listprofesor';
import { LoginProvider } from '../../providers/login/login';


@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})

export class ListPage {
  @ViewChild(Nav) nav:Nav;

  pages : [any] = null;

  ProfePages : [any] = [
      { title: "Home",
        page: ProfesorPage},

      { title: "Avisos Profesor",
        page: AvisoprofePage},

      { title: "Listado Profesores",
        page: ListprofesorPage,}
  ];

  EstudiPages : [any] = [
      { title: "Home",
        page: AlumnoPage},

      { title: "Listado Estudiantes",
        page: ListalumnoPage,
        icon: "ios-list-box-outline" },

      { title: "Grafico Curso",
        page: GrafalumnoPage,
        icon: "" }
  ];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loginprovider: LoginProvider){}
    
  
  goPage(page:any){
    this.navCtrl.setRoot(page);
  }

  ionViewDidEnter(){
    console.log(this.loginprovider.role);
    if(this.loginprovider.role === "Docente"){ 
      this.pages = this.ProfePages;
      this.navCtrl.setRoot(ProfesorPage);
    }
    else{
      this.pages = this.EstudiPages ;
      this.navCtrl.push(AlumnoPage);
    } 
  }

  logout(){
    this.navCtrl.setRoot(HomePage);
  }
}
