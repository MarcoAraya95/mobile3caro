import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { AlumnoPage } from '../pages/alumno/alumno';
import { ProfesorPage } from '../pages/profesor/profesor';
import { AvisoprofePage } from '../pages/avisoprofe/avisoprofe';
import { ForgotPage } from '../pages/forgot/forgot';
import { GrafalumnoPage } from '../pages/grafalumno/grafalumno';
import { ListalumnoPage } from '../pages/listalumno/listalumno';
import { ListprofesorPage } from '../pages/listprofesor/listprofesor';

import { LoginProvider } from './../providers/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    AlumnoPage,
    ProfesorPage,
    AvisoprofePage,
    ForgotPage,
    GrafalumnoPage,
    ListalumnoPage,
    ListprofesorPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ChartsModule,
    HttpClientModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    AlumnoPage,
    ProfesorPage,
    AvisoprofePage,
    ForgotPage,
    GrafalumnoPage,
    ListalumnoPage,
    ListprofesorPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LoginProvider,
    
  ]
})
export class AppModule {}
